#!/usr/bin/bash
#=================================================
#
# Rudimentary script to build docker images locally
#
#=================================================

set -ex

docker info
cd ../


# Build frontend
cd src/welpfrontend
FRONTEND_IMAGE_TAG="welpfrontend:local-build"
# Build docker image
docker build -t $FRONTEND_IMAGE_TAG .

# Go back to root
cd ../../

# Build backend
BACKEND_IMAGE_TAG="welpbackend:local-build"
cd src/welpbackend
docker build -t $BACKEND_IMAGE_TAG .
