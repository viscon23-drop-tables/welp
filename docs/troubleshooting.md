# Troubleshooting


## SSH auth errors

```
ssh -i id_rsa_13 admin@team-13.viscon-hackathon.ch
```

```
$ ssh -i ~/.ssh/id_rsa_13 admin@team-13.viscon-hackathon.ch
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0644 for '/home/gecko/.ssh/viscon23_id_rsa_13' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
Load key "/home/gecko/.ssh/viscon23_id_rsa_13": bad permissions
admin@team-13.viscon-hackathon.ch: Permission denied (publickey).
$
```

Change perms with
```
chmod 400 ~/.ssh/id_rsa_13
```
