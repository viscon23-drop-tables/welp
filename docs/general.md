# General

These docs us [mdbook](https://rust-lang.github.io/mdBook/) for rendering.

## Provided VPS

Server URL
```
team-13.viscon-hackathon.ch
```

Access server with
```
ssh -i id_rsa_13 admin@team-13.viscon-hackathon.ch
```

### Config

nginx config location

```
/etc/nginx/sites-available/default
```

Edit with

```
sudoedit /etc/nginx/sites-available/default
```

reload nginx with
```
sudo systemctl reload nginx
```
