import classNames from 'classnames'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { AccountIcon, BowlFoodIcon, UstensilsIcon } from './icons'

const tabs = [
  {
    name: 'Home',
    icon: BowlFoodIcon,
    href: '/home',
  },
  {
    name: 'List',
    icon: UstensilsIcon,
    href: '/restaurants',
  },
  {
    name: 'Account',
    icon: AccountIcon,
    href: '/account',
  },
]

export default function Navbar({ className }: { className?: string }) {
  const router = useRouter()

  const { pathname } = router
  return (
    <div
      className={classNames(
        'w-full flex flex-row justify-between px-12 py-4',
        'bg-gray-100',
        className
      )}
    >
      {tabs.map((tab, index) => (
        <Link key={index} href={tab.href}>
          <div>
            {
              <tab.icon
                className={classNames(
                  'h-8 w-8',
                  pathname.startsWith(tab.href)
                    ? 'fill-[--teal]'
                    : 'fill-transparent stroke-black stroke-[20]'
                )}
              />
            }
          </div>
        </Link>
      ))}
    </div>
  )
}
