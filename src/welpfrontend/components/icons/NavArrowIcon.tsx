export default function NavArrowIcon({ className }: { className?: string }) {
    return (
      <svg
        width={15}
        viewBox="0 0 13 13" 
        xmlns="http://www.w3.org/2000/svg"
      >

      <path d="M11.2868 0.089339L0.729927 4.9618C-0.488061 5.5303 -0.0820646 7.31678 1.21717 7.31678H5.68364V11.7832C5.68364 13.0825 7.47012 13.4887 8.03862 12.2705L12.9111 1.71358C13.3171 0.738831 12.2613 -0.316911 11.2868 0.089339Z" fill="black"/>
      </svg>
    )
  }
  