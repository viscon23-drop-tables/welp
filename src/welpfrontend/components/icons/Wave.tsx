export default function Wave({ className }: { className?: string }) {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 961 155"
        className={className}
      >
        <path d="M202.352 0C145.38 0 111.593 84.1093 0 84.1093V112.746V154.885H961V109.5V6.47737e-05C903.578 -6.47737e-05 860.101 90.5792 803.129 90.5792C731.914 90.5792 692.541 0 609.457 0C526.373 0 494.925 84.1093 403.928 84.1093C308.975 84.1093 269.544 -1.18389e-10 202.352 0Z"/>
      </svg>
    )
  }