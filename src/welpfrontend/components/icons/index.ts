import UstensilsIcon from './UstensilsIcon'
import AccountIcon from './AccountIcon'
import BowlFoodIcon from './BowlFoodIcon'
import MagnifyingLensIcon from './MagnifyingLensIcon'
import LocationDotIcon from './LocationDotIcon'
import ArrowLeftIcon from './ArrowLeftIcon'
import ArrowLeftLongIcon from './ArrowLeftLongIcon'
import CheckIcon from './CheckIcon'

export {
  UstensilsIcon,
  AccountIcon,
  BowlFoodIcon,
  MagnifyingLensIcon,
  LocationDotIcon,
  ArrowLeftIcon,
  ArrowLeftLongIcon,
  CheckIcon,
}
