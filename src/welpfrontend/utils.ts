export const range = (start: number, end?: number, step?: number) => {
  if (end === undefined) {
    end = start
    start = 0
  }

  if (step === undefined) {
    step = 1
  }

  const result = []

  for (let i = start; i < end; i += step) {
    result.push(i)
  }

  return result
}
