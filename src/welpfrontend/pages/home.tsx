import Navbar from '@/components/Navbar'
import classNames from 'classnames'
import { useEffect, useRef, useState } from 'react'
import 'react-spring-bottom-sheet/dist/style.css'
import Image from 'next/image'

import {
  getAllMensas,
  getAllMenus,
  Meal,
  MensasData,
  MenusData,
} from '@/api/data'

import { BottomSheet } from 'react-spring-bottom-sheet'
import BurgerIcon from '@/components/icons/BurgerIcon'

// const filters = ['Vegan', 'Vegetarian']
const filters = [
  {
    type: 'location',
    name: 'ETH Zentrum',
  },
  {
    type: 'diet',
    name: 'Vegetarian',
  },
  {
    type: 'diet',
    name: 'Meat',
  },
  {
    type: 'sort',
    name: 'By distance',
  },
  {
    type: 'sort',
    name: 'By rating',
  },
]

export default function Home() {
  // const focusRef = useRef<HTMLButtonElement>()
  // const sheetRef = useRef<BottomSheetRef>()
  const navbarRef = useRef<HTMLDivElement>(null)
  const headingRef = useRef<HTMLHeadingElement>(null)
  const widgetRef = useRef<HTMLDivElement>(null)

  // Data states
  const [mensas, setMensas] = useState<MensasData | null>(null)
  const [menus, setMenus] = useState<MenusData | null>(null)
  const [lat, setLat] = useState<number | null>(null)
  const [lon, setLon] = useState<number | null>(null)

  const [top3Meals, setTop3Meals] = useState<Meal[]>([])
  const [top3VeggieMeals, setTop3VeggieMeals] = useState<Meal[]>([])
  const [top3CloseMeals, setTop3CloseMeals] = useState<Meal[]>([])

  const [meals, setMeals] = useState<Meal[]>([])

  const [ready, setReady] = useState(false)

  const widgets = [
    {
      name: 'Top 3 Meals',
      state: top3Meals,
    },
    {
      name: 'Top 3 Veggie Meals',
      state: top3VeggieMeals,
    },
    {
      name: 'Top 3 Close Meals',
      state: top3CloseMeals,
    },
  ]

  useEffect(() => {
    if (ready) {
      console.log(navbarRef.current!.clientHeight)
    }
  }, [navbarRef, ready])

  // On load, get all mensas
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(function (position) {
      console.log('Latitude is :', position.coords.latitude)
      setLat(position.coords.latitude)
      console.log('Longitude is :', position.coords.longitude)
      setLon(position.coords.longitude)
    },
      function (error) {
        console.error('Error Code = ' + error.code + ' - ' + error.message)
        setTop3Meals(menus?.getTop3() || [])
        setTop3VeggieMeals(menus?.getTop3Veggie() || [])
        setMeals(menus?.getMeals() || [])
      }
    )

    getAllMensas().then((mensas) => {
      setMensas(mensas)
      console.log(mensas)
    })
  }, [])

  // Once Mensas have been loaded, get menus
  useEffect(() => {
    if (mensas) {
      // Get menus
      getAllMenus(mensas).then((menus) => {
        setMenus(menus)
        console.log(menus)
      })
    }
  }, [mensas])

  useEffect(() => {
    console.log(top3CloseMeals)
    console.log(top3Meals)
    console.log(top3VeggieMeals)

    // check if all values are set
    if (
      top3Meals.length > 0 &&
      top3VeggieMeals.length > 0 &&
      top3CloseMeals.length > 0
    ) {
      setReady(true)
    }
  }, [top3Meals, top3VeggieMeals, top3CloseMeals])

  // Once menus have been loaded, get distances
  useEffect(() => {
    if (mensas && menus) {
      // refresh distances
      if (lat && lon) mensas.updateDistances(lat, lon);
      setTop3CloseMeals(menus?.getTop3ByDistance() || [])
      // find top 3 meals
      setTop3Meals(menus?.getTop3() || [])
      setTop3VeggieMeals(menus?.getTop3Veggie() || [])
      setMeals(menus?.getMeals() || [])
    }
  }, [lat, lon, mensas, menus])

  const [widgetPage, setWidgetPage] = useState(0)

  const [locationFilter, setLocationFilter] = useState<string | null>(null)
  const [dietFilter, setDietFilter] = useState<string | null>(null)
  const [sortFilter, setSortFilter] = useState<string | null>(null)

  // Filtering
  useEffect(() => {
    var meals = menus?.getMeals() || []

    if (locationFilter === 'ETH Zentrum')
      meals = meals.filter((meal) => meal.mensa?.campus === 'zentrum')
    if (dietFilter) {
      if (dietFilter === 'Vegetarian') {
        meals = meals.filter((meal) => meal.is_vegetarian)
      } else if (dietFilter === 'Meat') {
        meals = meals.filter((meal) => !meal.is_vegetarian)
      }
    }
    if (sortFilter) {
      if (sortFilter === 'By distance') {
        meals = meals.sort((a, b) => {
          const dist_a = a.mensa?.distance || null
          const dist_b = b.mensa?.distance || null
          if (dist_a && dist_b) {
            return dist_a - dist_b
          }
          else {
            return 0
          }
        })
      }
      else if (sortFilter === 'By rating') {
        // meal?.score.toFixed(2)
        meals = meals.sort((a, b) => {
          var score_a = a?.score || null
          var score_b = b?.score || null
          {/* Calculation to increase variance a lot */ }
          score_a = score_a * 20 % 5
          score_b = score_b * 20 % 5
          if (score_a && score_b) {
            return score_b - score_a
          }
          else {
            return 0
          }
        })
      }

    }
    setMeals(meals)
  }, [locationFilter, dietFilter, sortFilter])

  if (!ready) {
    return (
      <div className="flex flex-col items-center justify-center h-screen">
        <h1 className="text-4xl font-bold">Loading...</h1>
      </div>
    )
  }

  return (
    <>
      <div ref={widgetRef}>
        <div
          className="flex flex-row gap-8 overflow-x-scroll px-8 py-8 no-scrollbar snap-x snap-mandatory"
          onScroll={(e) => {
            if (e.currentTarget.scrollLeft % (window.innerWidth - 32) === 0) {
              setWidgetPage(
                e.currentTarget.scrollLeft / (window.innerWidth - 32)
              )
            }
          }}
        >
          {widgets.map((widget, index) => (
            <div
              key={index}
              className="w-full shrink-0 snap-center space-y-4 bg-white shadow-[0_0_25px_-5px_rgba(0,0,0,0.25)] rounded-2xl px-4 py-4"
            >
              <h3 className="text-2xl font-semibold">{widget.name}</h3>

              <div className="flex flex-col gap-4">
                {widget.state.map((meal, index) => (
                  // <Link href={`/review/${restaurantId}/${meal.id}`} key={index}>
                  <div
                    key={index}
                    className="bg-gray-200 rounded-xl w-full flex flex-row gap-2 items-center overflow-hidden py-4 pl-2"
                  >
                    {meal.image ? (
                      <Image
                        src={meal.image}
                        width={100}
                        height={100}
                        alt={meal.name}
                      />
                    ) : (
                      <Image
                        src={'/images/' + (index + 1) + '.png'}
                        width={80}
                        height={80}
                        alt="Meal placeholder"
                        className="!p-2 !rounded-2xl"
                      />
                    )}
                    <div className="me-4 w-full">
                      <p className="text-base font-light">{meal.mensa?.name}</p>
                      <p className="text-xl font-semibold">{meal.name}</p>
                      <p className="font-light">
                        <span className="font-medium">
                          {meal.price_student +
                            ' / ' +
                            meal.price_internal +
                            ' / ' +
                            meal.price_external}
                        </span>{' '}
                        CHF
                      </p>
                    </div>
                  </div>
                  // </Link>
                ))}
              </div>
            </div>
          ))}
        </div>

        <div className="flex flex-row gap-4 justify-center">
          {widgets.map((widget, index) => (
            <div
              key={index}
              className={classNames(
                'h-3 rounded-full transition-all duration-300 delay-0 ease-out',
                widgetPage === index ? 'bg-gray-500 w-8' : 'bg-gray-300 w-3'
              )}
            />
          ))}
        </div>
      </div>

      <div
        ref={navbarRef}
        className="absolute z-50 left-0 right-0 bottom-0 shadow-[0_-20px_25px_-12px_rgba(0,0,0,0.2)] border-t border-gray-100"
      >
        <Navbar className="bg-white" />
      </div>
      <BottomSheet
        open
        skipInitialTransition
        snapPoints={({ maxHeight }) => [
          maxHeight,
          Math.max(
            maxHeight - widgetRef.current!.clientHeight - 32,
            navbarRef.current!.clientHeight +
            headingRef.current!.clientHeight +
            32
          ),
        ]}
        expandOnContentDrag
        blocking={false}
      >
        <div>
          <h1 ref={headingRef} className="text-2xl font-bold px-4 py-3">
            All Menus
          </h1>
          <div className="flex flex-row gap-2 overflow-x-scroll no-scrollbar px-4 flex-wrap">
            {filters.map((filter, index) => (
              <div
                key={index}
                className={classNames(
                  'border-[--teal] border text-[--teal] font-bold rounded-xl px-3 py-2 shrink-0',
                  (filter.type === 'location' &&
                    locationFilter === filter.name) ||
                    (filter.type === 'diet' && dietFilter === filter.name) ||
                    (filter.type === 'sort' && sortFilter === filter.name)
                    ? 'bg-[--teal] !text-white'
                    : 'bg-white'
                )}
                onClick={() => {
                  if (filter.type === 'location') {
                    if (locationFilter === filter.name) {
                      setLocationFilter(null)
                    } else {
                      setLocationFilter(filter.name)
                    }
                  } else if (filter.type === 'diet') {
                    if (dietFilter === filter.name) {
                      setDietFilter(null)
                    } else {
                      setDietFilter(filter.name)
                    }
                  } else {
                    // sort
                    if (filter.type === 'sort') {
                      if (sortFilter !== filter.name) {
                        setSortFilter(filter.name)
                      } else {
                        setSortFilter(null)
                      }
                    }
                  }
                }}
              >
                {filter.name}
              </div>
            ))}
          </div>
          <div className="flex flex-col gap-8 px-4 pt-4 overflow-hidden mb-20">
            {meals.map((meal, index) => (
              <div
                key={index}
                className="bg-gray-200 rounded-xl w-full flex flex-row gap-4 items-center overflow-hidden relative"
              >
                {meal.image ? (
                  <Image
                    src={meal.image}
                    width={100}
                    height={100}
                    alt={meal.name}
                  />
                ) : (
                  <Image
                    src="/images/ramen.jpg"
                    width={140}
                    height={140}
                    alt="Meal placeholder"
                    className="!p-2 !rounded-2xl flex-none h-full w-full"
                  />
                )}

                <div className="mr-4 shrink py-3 w-full me-15">
                  <p className="text-base font-light me-20">
                    {meal.mensa?.name}
                  </p>
                  <p className="text-xl font-semibold pt-2">{meal.name}</p>
                  <p className="font-light">
                    <span className="font-medium">
                      {meal.price_student +
                        ' / ' +
                        meal.price_internal +
                        ' / ' +
                        meal.price_external}
                    </span>{' '}
                    CHF
                  </p>
                </div>

                {(meal != null && meal.distance()) ? (
                  <>
                    <div className="absolute top-0 right-0 bg-[--teal] text-white font-bold rounded-bl-xl px-3 py-2">
                      {meal?.distance() > 1000
                        ? (meal?.distance() / 1000).toFixed(1) + ' km'
                        : meal.distance().toFixed(0) + ' m'}
                    </div>
                    <div className="absolute bottom-0 right-0 bg-[--teal] text-white font-bold rounded-tl-xl px-3 py-2">
                      <BurgerIcon />
                      {/* Calculation to increase variance a lot */}
                      {(meal?.score * 20 % 5).toFixed(1)}
                    </div>
                  </>
                ) : null}
              </div>
            ))}
          </div>
        </div>
      </BottomSheet>
    </>
  )
}
