import { Meal, Mensa, getAllMensas, getAllMenus } from '@/api/data'
import Navbar from '@/components/Navbar'
import { ArrowLeftIcon, LocationDotIcon } from '@/components/icons'
import classNames from 'classnames'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

const date = new Date()

const isRestaurantOpen = (restaurant: Mensa, date: Date) => {
  console.log(restaurant)
  if (!restaurant.primary_opening_time || !restaurant.primary_closing_time) {
    return false
  }
  const [primary_opening_time_hours, primary_opening_time_minutes] =
    restaurant.primary_opening_time.split(':', 2)
  const [primary_closing_time_hours, primary_closing_time_minutes] =
    restaurant.primary_closing_time.split(':', 2)

  if (
    date.getHours() >= Number(primary_opening_time_hours) &&
    date.getMinutes() >= Number(primary_opening_time_minutes) &&
    date.getHours() <= Number(primary_closing_time_hours) &&
    date.getMinutes() <= Number(primary_closing_time_minutes)
  ) {
    return true
  }

  if (
    !restaurant.secondary_opening_time ||
    !restaurant.secondary_closing_time
  ) {
    return false
  }
}

export default function Restaurants() {
  const router = useRouter()
  const { id: restaurantId } = router.query as { id: string }

  const [restaurant, setRestaurant] = useState<Mensa | null>()

  const [meals, setMeals] = useState<Meal[]>([])

  useEffect(() => {
    getAllMensas().then((data) => {
      const mensa = data.mensas.find(
        (mensa) => mensa.id === Number(restaurantId)
      )

      if (!mensa) {
        return
      }

      setRestaurant(mensa)

      getAllMenus(data).then((data) => {
        setMeals(
          data.menus.find((menu) => menu.mensa.id === mensa.id)?.meals || []
        )
      })
    })
  }, [restaurantId])

  if (!restaurant) {
    return <div>Loading...</div>
  }

  return (
    <>
      
      <div className="z-10 w-fit mt-4 ml-3">
        <div className='z-10 absolute rounded-xl bg-[--bg-dark] opacity-90 flex'>
        <ArrowLeftIcon className="w-6 h-6 ml-1 fill-white opacity-1" />
        <p className="font-['DM_Serif_Display'] text-white opacity-1 text-lg mr-2">go back.</p>
        </div>
        <Link href="/restaurants">
          <div
            className={classNames(
              'absolute z-10 w-fit backdrop-blur opacity-100',
              'flex flex-row gap-2 items-center',
              'border rounded-xl border-white shrink-0 '
            )}
          >
            <ArrowLeftIcon className="w-6 h-6 ml-1 fill-white opacity-100" />
            <p className="font-['DM_Serif_Display'] text-white opacity-100 text-lg mr-2">go back.</p>
          </div>
        </Link>
      </div>
      
      <img 
      src={`/images/mensas/${restaurantId}.jpg`} 
      alt="Picture of the mensa"
      className='-translate-y-4 object-cover h-auto w-full min-h-96'
      />
      <div className="mt-5 bg-[--bg-light]">
        <div>
          <div className="flex flex-row justify-center items-start gap-2">
            <LocationDotIcon className="w-6 h-6" />
            <h2 className="text-2xl">
              {restaurant.organization} {restaurant.campus}
            </h2>
          </div>
          <h1 className="text-5xl text-center">{restaurant.name}</h1>
        </div>

        <div className="flex-1 h-px bg-[--bg-medium-dark] my-2" />
        <div className='flex justify-center'>
          <p className=''>{isRestaurantOpen(restaurant, date) ? 'Open' : 'Closed'} {restaurant.distance}</p>
        </div>
        <div className="flex-1 h-px bg-[--bg-medium-dark] my-2" />

        <div className="px-4">
          <h3 className="text-2xl font-bold my-1">Meals of Today</h3>

          <div className="flex flex-col gap-4 mb-24">
            {meals.length === 0 && <p>No meals today</p>}

            {meals.map((meal, index) => (
              // <Link href={`/review/${restaurantId}/${meal.id}`} key={index}>
              <div
                key={index}
                className="bg-gray-200 rounded-xl w-full shrink-0 flex flex-row gap-4 items-center overflow-hidden"
              >
                {meal.image ? (
                  <Image
                    src={meal.image}
                    width={100}
                    height={100}
                    alt={meal.name}
                  />
                ) : (
                  <Image
                    src="/images/meal-placeholder.png"
                    width={100}
                    height={100}
                    alt="Meal placeholder"
                    className="!p-2 !rounded-2xl"
                  />
                )}
                <div className="mr-4">
                  <p className="text-xl font-semibold">{meal.name}</p>
                  <p className="font-light">
                    <span className="font-medium">{meal.price_student}</span>{' '}
                    CHF
                  </p>
                </div>
              </div>
              // </Link>
            ))}
          </div>
        </div>

        <div className="fixed z-50 left-0 right-0 bottom-0 shadow-[0_-20px_25px_-12px_rgba(0,0,0,0.2)] border-t border-gray-100">
          <Navbar className="bg-white" />
        </div>
      </div>
    </>
  )
}
