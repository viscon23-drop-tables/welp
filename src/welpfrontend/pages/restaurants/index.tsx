import classNames from 'classnames'
import Image from 'next/image'
import { useEffect, useRef, useState } from 'react'
import 'react-spring-bottom-sheet/dist/style.css'

import { BottomSheet, BottomSheetRef } from 'react-spring-bottom-sheet'
import Navbar from '@/components/Navbar'
import Link from 'next/link'
import { MagnifyingLensIcon } from '@/components/icons'

import fuzzysort from 'fuzzysort'
import { Mensa, getAllMensas } from '@/api/data'

// const locations = ['ETH Zentrum', 'ETH Hönggerberg', 'UZH Irchel']
const locations = [
  {
    organization: 'ETH',
    campus: 'zentrum',
    displayName: 'ETH Zentrum',
  },
  {
    organization: 'ETH',
    campus: 'honggerberg',
    displayName: 'ETH Hönggerberg',
  },
  {
    organization: 'UZH',
    campus: 'irchel',
    displayName: 'UZH Irchel',
  },
]

const restaurants = [
  { name: 'Polyterrasse', location: 'ETH Zentrum', open: false, id: '1' },
  { name: 'UZH Mensa', location: 'ETH Zentrum', open: false, id: '2' },
  { name: 'Polyterrasse', location: 'ETH Hönggerberg', open: true, id: '4' },
  { name: 'Polysnack', location: 'ETH Zentrum', open: true, id: '3' },
]

const openings = ['Open now']

const example = [...restaurants, ...restaurants]

const isRestaurantOpen = (restaurant: Mensa, date: Date) => {
  console.log(restaurant)
  if (!restaurant.primary_opening_time || !restaurant.primary_closing_time) {
    return false
  }
  const [primary_opening_time_hours, primary_opening_time_minutes] =
    restaurant.primary_opening_time.split(':', 2)
  const [primary_closing_time_hours, primary_closing_time_minutes] =
    restaurant.primary_closing_time.split(':', 2)

  if (
    date.getHours() >= Number(primary_opening_time_hours) &&
    date.getMinutes() >= Number(primary_opening_time_minutes) &&
    date.getHours() <= Number(primary_closing_time_hours) &&
    date.getMinutes() <= Number(primary_closing_time_minutes)
  ) {
    return true
  }

  if (
    !restaurant.secondary_opening_time ||
    !restaurant.secondary_closing_time
  ) {
    return false
  }

  const [secondary_opening_time_hours, secondary_opening_time_minutes] =
    restaurant.secondary_opening_time.split(':', 2)
  const [secondary_closing_time_hours, secondary_closing_time_minutes] =
    restaurant.secondary_closing_time.split(':', 2)

  if (
    date.getHours() >= Number(secondary_opening_time_hours) &&
    date.getMinutes() >= Number(secondary_opening_time_minutes) &&
    date.getHours() <= Number(secondary_closing_time_hours) &&
    date.getMinutes() <= Number(secondary_closing_time_minutes)
  ) {
    return true
  }

  return false
}

export default function Home() {
  const mainRef = useRef<HTMLDivElement>(null)
  const sheetRef = useRef<BottomSheetRef>(null)
  const searchDivRef = useRef<HTMLDivElement>(null)
  const searchInputRef = useRef<HTMLInputElement>(null)

  const [height, setHeight] = useState(0)

  useEffect(() => {
    setHeight(mainRef.current!.clientHeight)
  }, [setHeight])

  const [locationFilter, setLocationFilter] = useState<{
    organization: string
    campus: string
    displayName: string
  } | null>(null)
  const [openingFilter, setOpeningFilter] = useState<string | null>(null)

  const [searchInput, setSearchInput] = useState('')
  const [isSearching, setIsSearching] = useState(false)

  useEffect(() => {
    if (isSearching) {
      sheetRef.current?.snapTo(({ maxHeight }) => maxHeight)
      searchInputRef.current?.focus()
    } else {
      sheetRef.current?.snapTo(({ maxHeight }) => maxHeight - height)
      searchInputRef.current?.blur()
      setSearchInput('')
    }
  }, [isSearching, sheetRef, searchInputRef, height])

  const [restaurants, setRestaurants] = useState<Mensa[]>([])

  useEffect(() => {
    getAllMensas().then((data) => {
      setRestaurants(data.mensas)
    })
  }, [])

  const date = new Date()

  return (
    <>
      <div className="pb-4" ref={mainRef}>
        <div className='flex w-full justify-center items-center ml-4'>
          <h2 className={classNames(
            "px-4 py-3 font-bold text-5xl text-[--font-on-light]",
            isSearching ? 'w-0 opacity-0' : 'w-auto'
          )}>
            Restaurant
          </h2>
          <div ref={searchDivRef} className="py-4 px-4 relative w-full -translate-x-5">
            <input
              ref={searchInputRef}
              type="text"
              value={searchInput}
              onChange={(e) => {
                setSearchInput(e.target.value)
              }}
              className={classNames(
                'w-full h-12 bg-gray-50 rounded-lg px-4 py-4',
                'transition-all duration-300 ease-out delay-0',
                isSearching ? 'opacity-100' : 'opacity-0'
              )}
              placeholder="Which restaurant are you looking for?"
            />
            <div
              className="absolute right-8 top-0 bottom-0 mx-0 my-auto h-6 cursor-pointer"
              onClick={() => setIsSearching(true)}
            >
              <MagnifyingLensIcon className="w-6 h-6 fill-gray-500" />
            </div>
          </div>
        </div>


        <div className="space-y-3">
          <div className="space-y-1">
            <p className="text-sm ml-8">Location</p>
            <div className="flex flex-row gap-4 overflow-x-scroll no-scrollbar px-8">
              {locations.map((location, index) => (
                <div
                  key={index}
                  className={classNames(
                    'border-slate-800 border text-slate-600 rounded-xl px-3 py-2 shrink-0',
                    locationFilter === location
                      ? 'bg-[--teal] !text-white'
                      : 'bg-white'
                  )}
                  onClick={() => {
                    if (locationFilter === location) {
                      setLocationFilter(null)
                    } else {
                      setLocationFilter(location)
                    }
                  }}
                >
                  {location.displayName}
                </div>
              ))}
            </div>
          </div>

          <div className="space-y-1">
            <p className="text-sm ml-8">Openings</p>
            <div className="flex flex-row gap-4  overflow-x-scroll no-scrollbar  px-8">
              {openings.map((opening, index) => (
                <div
                  key={index}
                  className={classNames(
                    'border-slate-800 border text-slate-600 rounded-xl px-3 py-2 shrink-0',
                    openingFilter === opening
                      ? 'bg-[--teal] !text-white'
                      : 'bg-white'
                  )}
                  onClick={() => {
                    if (openingFilter === opening) {
                      setOpeningFilter(null)
                    } else {
                      setOpeningFilter(opening)
                    }
                  }}
                >
                  {opening}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>

      <div className="absolute z-50 left-0 right-0 bottom-0 shadow-[0_-20px_25px_-12px_rgba(0,0,0,0.2)] border-t border-gray-100">
        <Navbar className="bg-white" />
      </div>

      <BottomSheet
        ref={sheetRef}
        open
        skipInitialTransition
        snapPoints={({ maxHeight }) =>
          isSearching
            ? [
              maxHeight - searchDivRef.current!.clientHeight,
              maxHeight - height,
            ]
            : [maxHeight, maxHeight - height]
        }
        expandOnContentDrag
        blocking={false}
        onSpringStart={(event) => {
          if (
            isSearching &&
            event.type === 'SNAP' &&
            event.source === 'dragging'
          ) {
            setIsSearching(false)
          }
        }}
      >
        <div className="flex flex-col gap-4 px-8 pt-4 pb-20">
          {restaurants
            .filter((restaurant) =>
              !isSearching && locationFilter
                ? restaurant.campus === locationFilter.campus &&
                restaurant.organization === locationFilter.organization
                : true
            )
            .filter((restaurant) => {
              if (isSearching || !openingFilter) {
                return true
              }

              if (openingFilter === 'Open now') {
                return isRestaurantOpen(restaurant, date)
              }

              return true
            })
            .filter((restaurant) =>
              isSearching && searchInput.length > 0
                ? fuzzysort.single(searchInput, restaurant.name)
                : true
            )
            .map((restaurant, key) => (
              <Link href={`/restaurants/${restaurant.id}`} key={restaurant.id}>
                <div
                  key={key}
                  className="bg-gray-100 rounded-xl px-4 py-2 w-full shrink-0 flex flex-row justify-between items-center"
                >
                  <div>
                    <p className="text-xl font-normal">{restaurant.name}</p>
                    <p className="text-base font-light">
                      {
                        locations.find(
                          (l) =>
                            restaurant.campus == l.campus &&
                            restaurant.organization == l.organization
                        )?.displayName
                      }
                    </p>
                  </div>
                  <div className="flex flex-row gap-4">
                    <div
                      className={classNames(
                        'rounded-full px-3 py-2 text-sm text-white',
                        isRestaurantOpen(restaurant, date)
                          ? 'bg-green-500'
                          : 'bg-red-500'
                      )}
                    >
                      {isRestaurantOpen(restaurant, date) ? 'Open' : 'Closed'}
                    </div>
                    <Image
                      src="/icons/chevron-right-solid.svg"
                      width={16}
                      height={16}
                      alt="Chevron"
                    />
                  </div>
                </div>
              </Link>
            ))}
        </div>
      </BottomSheet>
    </>
  )
}
