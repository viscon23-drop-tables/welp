import Navbar from '@/components/Navbar'
import Wave from '@/components/icons/Wave'
import classNames from 'classnames'
import Link from 'next/link'
import { useState } from 'react'

export default function AccountPage() {
  const [isSignedIn, setIsSignedIn] = useState(false)
  return (
    <>
      <div className="h-screen flex flex-col absolute">
        <div className='font-["DM_Serif_Display"] bg-[--cream]"] w-screen'>
          <div className="flex flex-row gap-4 px-8 h-full py-40">
            <svg
              width="1"
              height="117"
              viewBox="0 0 1 117"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <line
                x1="0.508545"
                y1="117.008"
                x2="0.508545"
                y2="-0.00866699"
                stroke="black"
              />
            </svg>
            <div>
              <p className="text-6xl font-bold py-1">welp</p>
              <p className="text-3xl font-bolds">it&apos;s food I guess</p>
            </div>
          </div>
          <div
            className={classNames(
              'flex flex-row gap-6 w-full',
              'leading-none text-2xl font-bold',
              'items-center justify-center',
              'absolute top-3/4 z-10'
            )}
          >
            <p>
              write a <br></br>review
            </p>
            <p>•</p>
            <p>
              find a <br></br>meal
            </p>
          </div>
        </div>
        <div className="flex flex-col overflow-hidden h-full">
          <div className="">
            <Wave className="min-w-full min-w-200 fill-[--teal] top-1/2 z-0 translate-y-1" />
          </div>
          <div className="flex-1 min-w-full bg-[--teal]"></div>
        </div>
      </div>
      <div className="fixed flex w-screen h-full z-20">
        <Link href={'/review'}>
          <div className="relative w-1/2 h-1/2 top-1/2"></div>
        </Link>
        <Link href={'/home'}>
          <div className="relative w-1/2 h-1/2 top-1/2"></div>
        </Link>
      </div>
    </>
  )
}