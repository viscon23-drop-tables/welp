import { ArrowLeftLongIcon } from '@/components/icons'
import Wave from '@/components/icons/Wave'
import Link from 'next/link'
import { useState } from 'react'

export default function SignInPage() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  return (
    <div className='h-screen font-["DM_Serif_Display"]'>
      <Link href="/account">
        <button className="font-['DM_Serif_Display'] text-slate-900 text-3xl text-[#ff0rfa] absolute left-4 top-4 flex flex-row gap-2 items-center">
          <ArrowLeftLongIcon className="w-6 h-6" />
          go back
        </button>
      </Link>
      <div className="h-2/3 ">
        <div className="space-y-2 bg-[--teal] px-8 h-full flex flex-col justify-center">
          <h1 className='font-["DM_Serif_Display"] text-8xl mb-6'>sign in.</h1>
          <input
            type="text"
            placeholder="email"
            className='w-full px-4 py-3 rounded-full font-["DM_Serif_Display"] border border-slate-800 bg-[--bg-light]'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <input
            type="text"
            placeholder="password"
            className='w-full px-4 py-3 rounded-full font-["DM_Serif_Display"] border border-slate-800 bg-[--bg-light]'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Link href="/home">
            <button className='font-["DM_Serif_Display"] ml-auto !mt-4 text-4xl flex flex-row gap-2 items-center'>
              next <ArrowLeftLongIcon className="rotate-180 w-6 h-6" />
            </button>
          </Link>
        </div>
        <Wave className="min-w-full min-w-200 fill-[--teal] -translate-y-1 rotate-180" />
      </div>

      <div className="h-1/3 bg-[--bg-light] flex flex-col justify-center items-center">
        <div className="flex flex-col justify-center items-center">
          <p className="text-xl text-slate-400">don&apos;t have an account?</p>
          <Link href="/auth/signup">
            <p className="text-3xl font-normal ">sign up.</p>
          </Link>
        </div>
      </div>
    </div>
  )
}
