import Navbar from '@/components/Navbar'
import { ArrowLeftLongIcon, CheckIcon } from '@/components/icons'
import ChevronRight from '@/components/icons/ChevronRight'
import Link from 'next/link'
import { useEffect, useState } from 'react'

const MenuItem = ({
  title,
  selected,
  savePreset,
}: {
  title: string
  selected: boolean
  savePreset: (title: string) => void
}) => {
  return (
    <div
      className="flex flex-row justify-between bg-gray-50 px-4 items-center py-2"
      onClick={() => savePreset(title)}
    >
      <h1 className=" text-slate-900 text-xl">{title}</h1>
      {selected && <CheckIcon className="w-6 h-6 fill-[--teal]" />}
    </div>
  )
}

export default function PreferencesPage() {
  const [status, setStatus] = useState<string | null>(null)

  useEffect(() => {
    if (window.localStorage) {
      setStatus(window.localStorage.getItem('status'))
    }
  }, [])

  const savePreset = (title: string) => {
    if (window.localStorage) {
      window.localStorage.setItem('status', title)
    }
    setStatus(title)
  }

  return (
    <>
      <Link href="/account/preferences">
        <button className="font-['DM_Serif_Display'] text-slate-900 text-3xl text-[#ff0rfa] absolute left-4 top-4 flex flex-row gap-2 items-center">
          <ArrowLeftLongIcon className="w-6 h-6" />
          go back
        </button>
      </Link>
      <div className="flex flex-col justify-around h-full pt-16 gap-4">
        <h1 className="font-['DM_Serif_Display'] text-4xl mx-4">Status</h1>
        <Link href="/account/preferences/selection">
          <div className="divide-y divide-gray-200">
            <MenuItem
              title="Student"
              selected={status == 'Student'}
              savePreset={savePreset}
            />
            <MenuItem
              title="Staff"
              selected={status == 'Staff'}
              savePreset={savePreset}
            />
            <MenuItem
              title="External"
              selected={status == 'External'}
              savePreset={savePreset}
            />
          </div>
        </Link>
      </div>

      <div className="absolute z-50 left-0 right-0 bottom-0 border-t border-gray-100">
        <Navbar className="bg-white" />
      </div>
    </>
  )
}
