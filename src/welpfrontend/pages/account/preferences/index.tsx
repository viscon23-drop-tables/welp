import Navbar from '@/components/Navbar'
import { ArrowLeftLongIcon } from '@/components/icons'
import ChevronRight from '@/components/icons/ChevronRight'
import Link from 'next/link'
import { useEffect, useState } from 'react'

const MenuItem = ({
  title,
  subtitle,
}: {
  title: string
  subtitle: string | null
}) => {
  return (
    <div className="flex flex-row justify-between bg-gray-50 px-4 items-center py-2">
      <h1 className=" text-slate-900 text-xl">{title}</h1>
      <div className="flex flex-row gap-2 items-center">
        <p className="text-gray-400">{subtitle ?? 'Not define'}</p>
        <ChevronRight className="w-6 h-6" />
      </div>
    </div>
  )
}

export default function PreferencesPage() {
  const [status, setStatus] = useState<string | null>(null)

  useEffect(() => {
    if (window.localStorage) {
      setStatus(window.localStorage.getItem('status'))
    }
  }, [])

  return (
    <>
      <Link href="/account">
        <button className="font-['DM_Serif_Display'] text-slate-900 text-3xl text-[#ff0rfa] absolute left-4 top-4 flex flex-row gap-2 items-center">
          <ArrowLeftLongIcon className="w-6 h-6" />
          go back
        </button>
      </Link>
      <div className="flex flex-col justify-around h-full pt-16 gap-4">
        <h1 className="font-['DM_Serif_Display'] text-4xl mx-4">Preferences</h1>
        <Link href="/account/preferences/status">
          <div>
            <MenuItem title="Status" subtitle={status} />
          </div>
        </Link>
      </div>

      <div className="absolute z-50 left-0 right-0 bottom-0 border-t border-gray-100">
        <Navbar className="bg-white" />
      </div>
    </>
  )
}
