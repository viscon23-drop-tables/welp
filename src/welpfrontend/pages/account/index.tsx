import Navbar from '@/components/Navbar'
import CouponIcon from '@/components/icons/CouponIcon'
import LeaderboardIcon from '@/components/icons/LeaderboardIcon'
import LogoutIcon from '@/components/icons/LogoutIcon'
import MicrochipIcon from '@/components/icons/MicrochipIcon'
import SettingsIcon from '@/components/icons/SettingsIcon'
import Link from 'next/link'
import { useState } from 'react'

const MenuItem = ({
  icon: Icon,
  title,
  subtitle,
}: {
  icon: (props?: any) => JSX.Element
  title: string
  subtitle: string
}) => {
  return (
    <div className="flex flex-row gap-8 bg-gray-50 px-8 items-center py-2">
      <Icon className="w-12 h-12" />

      <div className="flex flex-col">
        <h1 className=" text-slate-900 text-3xl">{title}</h1>
        <h2 className=" text-slate-900 text-base font-thin">{subtitle}</h2>
      </div>
    </div>
  )
}

export default function AccountPage() {
  const [isSignedIn, setIsSignedIn] = useState(false)
  return (
    <>
      <div className="flex flex-col h-screen">
        <div className="flex flex-col w-full divide-y divide-gray-200">
          <Link href="/account/preferences">
            <div>
              <MenuItem
                icon={SettingsIcon}
                title="User Preferences"
                subtitle="location, notifications etc."
              />
            </div>
          </Link>
          <div>
            <MenuItem
              icon={MicrochipIcon}
              title="Stats"
              subtitle="reviews n stuff (coming soon)"
            />
          </div>
          <div>
            <MenuItem
              icon={LeaderboardIcon}
              title="Leaderboard"
              subtitle="Best of the best! (coming soon)"
            />
          </div>
          <div>
            <MenuItem
              icon={CouponIcon}
              title="Coupons"
              subtitle="Collect your Rewards! (coming soon)"
            />
          </div>
        </div>
        {/* <button className="font-['DM_Serif_Display'] text-slate-900 text-3xl text-[#ff0rfa]">
          go back.
        </button> */}
        <div className="flex flex-col justify-center items-center h-full">
          <Link href={isSignedIn ? '/auth/signout' : '/auth/signin'}>
            <h1 className="font-['DM_Serif_Display'] text-slate-900 text-5xl">
              {isSignedIn ? 'sign out.' : 'sign in.'}
            </h1>
          </Link>
        </div>
      </div>

      <div className="absolute z-50 left-0 right-0 bottom-0 border-t border-gray-100">
        <Navbar className="bg-white" />
      </div>
    </>
  )
}
