import { useEffect, useState } from 'react'
import Image from 'next/image'
import classNames from 'classnames'
import Link from 'next/link'
import ChevronRight from '@/components/icons/ChevronRight'
import 'react-spring-bottom-sheet/dist/style.css'
import { BottomSheet } from 'react-spring-bottom-sheet'
import BurgerIcon from '@/components/icons/BurgerIcon'
import NavArrowIcon from '@/components/icons/NavArrowIcon'

const restaurants = [
  { name: 'Polyterrasse', location: 'ETH Zentrum', rating: '3.2', distance: '45', open: false },
  { name: 'UZH Mensa', location: 'ETH Zentrum', rating: '4.3', distance: '245', open: false },
  { name: 'Polysnack', location: 'ETH Zentrum', rating: '4.1', distance: '100', open: true },
]

const example = [...restaurants, ...restaurants, ...restaurants, ...restaurants]

export default function ReviewPage() {
  const [restaurant, setRestaurant] = useState('Polyterrasse')
  const [location, setLocation] = useState('ETH Zentrum')

  const [search, setSearch] = useState('')
  const [height, setHeight] = useState(0)

  //   useEffect(() => {
  //     if (!navigator.geolocation) {
  //       setLocation('Geolocation is not supported by your browser')
  //       return
  //     }

  //     navigator.geolocation.getCurrentPosition(
  //       (position) => {
  //         // const accuracy = position.coords.accuracy
  //         setLocation(`${position.coords.latitude}, ${position.coords.longitude}`)
  //       },
  //       (e) => {
  //         setLocation('Unable to retrieve your location')
  //       }
  //     )
  //   }, [setLocation])

  return (
    <>
      <h2 className="px-4 py-3 font-bold text-5xl text-[--font-on-light]">Review</h2>
      
      <p className="text-3xl font-bold pl-4">Where did you eat?</p>
      <p className="font-bold pl-4 py-3">Based on your location:</p>

      <Link href={`/review/{${restaurant}`}>
        <div className="space-y-1 px-4">
          
          
          <div
            key={location}
            className={classNames(
              'bg-[--teal] rounded-xl px-4 py-2 shrink-0',
              'flex flex-row justify-between items-center',
              'text-white font-bold'
            )}
          >
            <div>
              <p className="text-xl">{restaurant}</p>
              <p>{location}</p>
            </div>
            <ChevronRight 
              className='w-4 fill-[--accent]'
            />
          </div>
        </div>
      </Link>

      <div className="flex flex-row items-center gap-4 my-2">
        <div className="flex-1 h-px bg-[--bg-medium-dark]" />
        <span className="unselectable text-[--bg-medium-dark]">Or</span>
        <div className="flex-1 h-px bg-[--bg-medium-dark]" />
      </div>

      <div className='px-4'>
        <input
          type="text"
          placeholder="Search restaurant..."
          className="bg-[--bg-medium] w-full px-4 py-2 rounded-xl"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
        

      <div className="flex flex-col gap-8 px-4 py-4">
        {example.map((restaurant, key) => (
          <Link href={`/review/${restaurant.name}`} key={key}>
            <div className="bg-[--bg-medium] rounded-xl px-4 py-2 w-full shrink-0 flex flex-row justify-between items-center">
              <div>
                <p className="text-xl">{restaurant.name}</p>
                <div className="flex flex-row gap-2 items-center">
                  <p>{restaurant.location}</p>
                  <p>•</p>
                  <p>{restaurant.rating}</p>
                  <BurgerIcon 
                    className='px-2 w-4 fill-[--accent]'
                    />
                  <p>•</p>
                  <p>{restaurant.distance}m</p>
                  <NavArrowIcon className=""/>
                </div>
              </div>

              <ChevronRight 
                className=' w-4 fill-[--accent]'
              />
            </div>
          </Link>
        ))}
      </div>
    </>
  )
}
