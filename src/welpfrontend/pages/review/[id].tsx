import { useEffect, useState } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Link from 'next/link'
import ChevronRight from '@/components/icons/ChevronRight'
import { ArrowLeftLongIcon } from '@/components/icons'
import classNames from 'classnames'

export default function RestaurantReviewPage() {
  const router = useRouter()
  const { id: restaurantId } = router.query as { id: string }

  const [meals, setMeals] = useState<
    { id: string; name: string; price: number }[]
  >([])

  useEffect(() => {
    async function getRestaurantMeals(restaurantId: string) {
      // const res = await fetch(
      //     `https://welp-backend.duckdns.org/api/restaurants/${restaurantId}/meals`
      // )

      // const json = await res.json()

      // console.log(json)

      setMeals([
        {
          id: '1',
          name: 'Pizza',
          price: 10,
        },
        {
          id: '2',
          name: 'Pasta',
          price: 8,
        },
        {
          id: '3',
          name: 'Salad',
          price: 5,
        },
      ])
    }

    getRestaurantMeals(restaurantId)
  }, [restaurantId])

  return (
    <>

      <h1 className="px-4 py-3 font-bold text-5xl text-[--font-on-light]">Review</h1>
      
      <p className="text-3xl font-bold pl-4">What did you eat?</p>

      <div className="flex flex-col gap-4 px-4 py-4">
        {meals.map((meal, index) => (
          <Link href={`/review/${restaurantId}/${meal.id}`} key={index}>
            <div className="bg-gray-200 rounded-xl px-4 py-2 w-full shrink-0 flex flex-row justify-between items-center">
              <div>
                <p className="text-xl">{meal.name}</p>
                <p>{meal.price} CHF</p>
              </div>

              <ChevronRight className='fill-[--accent] w-4'/>
            </div>
          </Link>
        ))}
      </div>
      <div className={classNames('h-screen font-["DM_Serif_Display"]')}>
        <Link href="/review">
          <button className="font-['DM_Serif_Display'] text-[--font-on-light] text-3xl absolute left-4 bottom-4 flex flex-row gap-2 items-center">
            <ArrowLeftLongIcon className="w-6 h-6 fill-[--font-on-light]" />
            go back
          </button>
        </Link>
      </div>
    </>
  )
}
