import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Image from 'next/image'
import classNames from 'classnames'
import { range } from '@/utils'
import BurgerIcon from '@/components/icons/BurgerIcon'
import { debug } from 'console'
import Link from 'next/link'
import NavArrowIcon from '@/components/icons/NavArrowIcon'
import { ArrowLeftLongIcon } from '@/components/icons'

const optionalRatings = [
  {
    name: 'Good service',
    icon: '/icons/thumbs-up-solid.svg',
  },
  {
    name: 'Good value',
    icon: '/icons/sack-dollar-solid.svg',
  },
  {
    name: 'Crowded',
    icon: '/icons/people-group-solid.svg',
  },
  {
    name: 'Large portion',
    icon: '/icons/bowl-food-solid.svg',
  },
  {
    name: 'Refill',
    icon: '/icons/arrow-rotate-left-solid.svg',
  },
]






export default function FinalPage() {
  const router = useRouter()

  const { slug } = router.query

  const restaurantId = slug ? slug[0] : ''
  const mealId = slug ? slug[1] : ''

  const [restaurant, setRestaurant] = useState<{
    id: string
    name: string
    location: string
  }>()

  const [rating, setRating] = useState(0);


  const [meal, setMeal] = useState<{
    id: string
    name: string
    price: number
  }>()

  useEffect(() => {
    async function getMeal(id: string) {
      setMeal({
        id: '1',
        name: 'Pizza',
        price: 10,
      })
    }

    async function getRestaurant(id: string) {
      setRestaurant({
        id: '1',
        name: 'Polyterrasse',
        location: 'ETH Zentrum',
      })
    }

    getRestaurant(restaurantId)
    getMeal(mealId)
  }, [restaurantId, mealId])
  
  

  return (
    <>
      <div className="flex flex-col items-center mt-3 drop-shadow-md">
        <Image
          src="/images/ramen.jpg"
          alt="Ramen"
          height={150}
          width={150}
          className="object-cover rounded-xl mask mask-squircle"
        />
        <h2 className="text-2xl">{meal?.name}</h2>
        <p className="text-gray-500">{restaurant?.name}</p>
      </div>

      <div className='mt-10 mx-4'>
        <p className="text-1xl font-bold mb-2">How was your meal?</p>
        <div className="flex flex-row justify-between">
          {range(5).map((i) => (
            <div key={i} onClick={()=>{setRating(i + 1)}}>
              <BurgerIcon className={classNames(
                'h-10',
                (i < rating) ? 'fill-[--accent]' : 'fill-black' 
              )}
              />
            </div>
          ))}
        </div>
      </div>

      <div className={classNames(
        "flex flex-row flex-wrap gap-2  justify-between",
        "mt-5 mx-4",
        (rating > 0) ? "opacity-100" : "opacity-0"
      )}>
        {optionalRatings.map((rating, index) => (
          <div
            key={index}
            className={classNames(
              'flex flex-row items-center gap-2',
              'bg-gray-300 rounded-xl px-4 py-2'
            )}
          >
            <Image src={rating.icon} alt={rating.name} height="16" width="16" />
            <p className='text-sm'>{rating.name}</p>
          </div>
        ))}
      </div>
      <div className={classNames('h-screen font-["DM_Serif_Display"]',
      (rating > 0) ? "opacity-100" : "opacity-0")}>
        <Link href="/">
          <button className="font-['DM_Serif_Display'] text-[--teal] text-3xl absolute right-4 bottom-4 flex flex-row gap-2 items-center">
            submit
            <ArrowLeftLongIcon className="w-6 h-6 rotate-180 fill-[--teal]" />
          </button>
        </Link>
      </div>
      <div className={classNames('h-screen font-["DM_Serif_Display"]')}>
        <Link href={`/review/${restaurantId}}`}>
          <button className="font-['DM_Serif_Display'] text-[--font-on-light] text-3xl absolute left-4 bottom-4 flex flex-row gap-2 items-center">
            <ArrowLeftLongIcon className="w-6 h-6 fill-[--font-on-light]" />
            go back
          </button>
        </Link>
      </div>
    </>
  )
}
