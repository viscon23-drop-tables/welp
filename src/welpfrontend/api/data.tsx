const ENDPOINT = 'https://team-13.viscon-hackathon.ch/api/'

// Get all Mensas
export const getAllMensas = async () => {
  const response = await fetch(ENDPOINT + 'mensas')
  const data = await response.json()
  console.log('getAllMensas', data)
  return new MensasData(data)
}

// Get all Menus
export const getAllMenus = async (mensas: MensasData) => {
  const response = await fetch(ENDPOINT + 'menus')
  const data = await response.json()
  console.log(data)
  return new MenusData(data, mensas)
}

export class Mensa {
  id: number
  name: string
  latitude: number
  longitude: number
  organization: string
  campus: string
  primary_opening_time: string | null
  primary_closing_time: string | null
  secondary_opening_time: string | null
  secondary_closing_time: string | null

  distance: number | null = null

  constructor(data: any, latitude?: number, longitude?: number) {
    this.id = data.id
    this.name = data.display_name
    this.latitude = data.location_x
    this.longitude = data.location_y
    this.organization = data.organization
    this.campus = data.campus
    this.primary_opening_time = data.primary_opening_time
    this.primary_closing_time = data.primary_closing_time
    this.secondary_opening_time = data.secondary_opening_time
    this.secondary_closing_time = data.secondary_closing_time

    if (latitude && longitude) {
      this.distance = getDistanceFromLatLonInM(
        latitude,
        longitude,
        this.latitude,
        this.longitude
      )
    }
  }

  updateDistance(latitude: number, longitude: number) {
    this.distance = getDistanceFromLatLonInM(
      latitude,
      longitude,
      this.latitude,
      this.longitude
    )
  }
}

export class MensasData {
  mensas: Mensa[]

  constructor(data: any) {
    this.mensas = data.map((mensa: any) => new Mensa(mensa))
  }

  updateDistances(latitude: number, longitude: number) {
    this.mensas.forEach((mensa: Mensa) =>
      mensa.updateDistance(latitude, longitude)
    )
  }
}

export class Meal {
  id: number
  menu_id: number
  name: string
  brand: string
  long_description: string
  price_student: number
  price_internal: number
  price_external: number
  is_vegetarian: boolean
  image: string | null

  score: number
  value_score: number
  service_score: number
  crowded_score: number
  refillable_score: number
  portion_score: number

  mensa: Mensa | null = null

  constructor(data: any, mensa?: Mensa) {
    this.id = data.id
    this.menu_id = data.menu_id

    // format name to have first letter uppercase and rest lowercase, must account for special characters
    this.name = data.name
      .toLowerCase()
      .replace(/^(.)|\s(.)/g, ($1: string) => $1.toUpperCase())
    this.brand = data.brand
    this.long_description = data.long_description
    this.price_student = data.price_student
    this.price_internal = data.price_internal
    this.price_external = data.price_external
    this.is_vegetarian = data.is_vegetarian
    this.image = data.image

    this.score = data.score
    this.value_score = data.value_score
    this.service_score = data.service_score
    this.crowded_score = data.crowded_score
    this.refillable_score = data.refillable_score
    this.portion_score = data.portion_score
    this.mensa = mensa || null
  }

  distance() {
    return this.mensa?.distance || null
  }
}

export class Menu {
  id: number
  mensa: Mensa
  meals: Meal[]

  constructor(data: any, mensas: MensasData) {
    this.id = data.id
    this.mensa = mensas.mensas.find(
      (mensa: Mensa) => mensa.id === data.associated_mensa.id
    )!
    this.meals = data.meals.map((meal: any) => new Meal(meal, this.mensa))
  }
}

export class MenusData {
  menus: Menu[]

  constructor(data: any, mensas: MensasData) {
    this.menus = data.map((menu: any) => new Menu(menu, mensas))
  }

  // get all meals
  getMeals() {
    return this.menus.flatMap((menu: Menu) => menu.meals)
  }

  // get ranked by score
  getRanked() {
    return this.getMeals().sort((a, b) => b.score - a.score)
  }

  // get top 3
  getTop3() {
    return this.getRanked().slice(0, 3)
  }

  getTop3Veggie() {
    return this.getMeals()
      .filter((meal: Meal) => meal.is_vegetarian)
      .sort((a, b) => b.score - a.score)
      .slice(0, 3)
  }

  // get top 3 by distance
  getTop3ByDistance() {
    return this.getByDistance().slice(0, 3)
  }

  getByDistance() {
    return this.getMeals().sort((a, b) => {
      const dist_a = a.mensa?.distance || null
      const dist_b = b.mensa?.distance || null

      if (dist_a == dist_b) {
        return b.score - a.score
      } else if (dist_a == null) {
        return 1
      } else if (dist_b == null) {
        return -1
      }
      return dist_a - dist_b
    })
  }
}

// Function to calculate distance between two coordinates in meters
function getDistanceFromLatLonInM(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) {
  const R = 6371 // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1) // deg2rad below
  const dLon = deg2rad(lon2 - lon1)
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  // Distance in km
  return R * c * 1000
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180)
}
