# %%
import requests
import json
import sqlite3
from datetime import datetime

def GetAllETHMenus():
    today = datetime.utcnow().strftime('%Y-%m-%d')
    url = f"https://idapps.ethz.ch/cookpit-pub-services/v1/weeklyrotas?client-id=ethz-wcms&lang=en&rs-first=0&rs-size=50&valid-after={today}"

    response = requests.get(url)

    if response.status_code != 200:
        return None

    responseJson = json.loads(response.content)

    with open('menu.json', 'w') as f:
        json.dump(responseJson, f)

    return responseJson

menus = GetAllETHMenus()
# %%
# example
menus["weekly-rota-array"][36]["day-of-week-array"][3]["opening-hour-array"][0]["meal-time-array"][0]["line-array"][0]["meal"]["name"]

# %%
# example 2
for x in menus["weekly-rota-array"][36]["day-of-week-array"][3]["opening-hour-array"][0]["meal-time-array"][0]["line-array"]:
    print(x["meal"]["name"])
# %%
# example 3
for x in menus["weekly-rota-array"][36]["day-of-week-array"][3]["opening-hour-array"][0]["meal-time-array"][0]["line-array"]:
    print(x["meal"]["name"])

# %%
for week in menus["weekly-rota-array"]:
    print(week["facility-id"])


# %%
# example 4
menus["weekly-rota-array"][36]["day-of-week-array"][3]["opening-hour-array"][0]["meal-time-array"][0]["line-array"][0]["meal"]["name"]

x = menus["weekly-rota-array"][36]
for day in x["day-of-week-array"]:
    for opening_hour in day["opening-hour-array"]:
        for meal_time in opening_hour["meal-time-array"]:
            for line in meal_time["line-array"]:
                print(line["meal"]["name"])
