from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import WelpUser

class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = WelpUser
        fields = ("username", "email")

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = WelpUser
        fields = ("username", "email")