from django.db import models
from django.contrib.auth.models import AbstractUser

class WelpUser(AbstractUser):
    """
    Custom user class
    """

    def review_count(self) -> int:
        """
        Returns the number of reviews this user has made.
        """
        return len(self.reviews.all())