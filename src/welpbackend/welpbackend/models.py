from django.db import models

class Mensa(models.Model):
    DisplayName = models.CharField(max_length=200, editable=True)
    LocationX = models.DecimalField(max_digits=22, decimal_places=16, blank=True, null=True, editable=True)
    LocationY = models.DecimalField(max_digits=22, decimal_places=16, blank=True, null=True, editable=True)
    Organization = models.CharField(max_length=200, editable=True)
    Campus = models.CharField(max_length=200, editable=True) #Höng or Zentrum

class Month(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()

class ReviewSession(models.Model):
    """
    Represents a review session for either a Lunch session or a dinner session, on a given day.
    
    """
    day_of_month = models.IntegerField()
    day_name = models.CharField(max_length=200)
    month = models.ForeignKey(Month, on_delete=models.CASCADE, name="day")

class Review(models.Model):
    """
    Represents a review of a specific meal, by a specific user, in a specific session.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, name="user")
    session = models.ForeignKey(ReviewSession, on_delete=models.CASCADE, name="session")
    rating = models.IntegerField()
    comment = models.CharField(max_length=200, editable=True)


class MenuEntry(models.Model):
    # Direct link to mensa
    mensa = models.ForeignKey(Mensa, on_delete=models.CASCADE, name="menu")


class Meal(models.Model):
    # Direct link to mensa

