import json
from django.shortcuts import render
from rest_framework import viewsets
from api.serializers import *
from api.models import Meal, Mensa
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

from accounts.models import WelpUser

class MensasViewSet(viewsets.ModelViewSet):
    queryset = Mensa.objects.all()
    serializer_class = MensaSerializer


class RankedMealViewSet(viewsets.ModelViewSet):
    serializer_class = MealSerializer

    def get_queryset(self):
        """
        Returns a queryset of all meals for the current session, ordered by score.
        """
        session = ReviewSession.get_current_session()
        if session is None:
            return Meal.objects.none()
        
        # convert to list
        meals = list(Meal.objects.filter(menu__session=session))
        # sort by score
        meals.sort(key=lambda meal: meal.score(), reverse=True)
        return meals
    
class MenusViewSet(viewsets.ModelViewSet):
    serializer_class = MenuSerializer

    def get_queryset(self):
        """
        Returns a queryset of all menus for the current session.
        """
        session = ReviewSession.get_current_session()
        if session is None:
            return Menu.objects.none()
        
        return Menu.objects.filter(session=session)
    

"""
POST review 
URL params:
    meal_id int REQUIRED
    rating float REQUIRED
    is_good_value bool OPTIONAL
    is_good_service bool OPTIONAL
    is_crowded bool OPTIONAL
    is_refillable bool OPTIONAL
    is_large_portion bool OPTIONAL
    user_id int OPTIONAL

"""
@csrf_exempt
def post_review(request):
    if request.method != 'POST':
        return HttpResponseBadRequest("Invalid method - Use POST")
    
    # Get parameters from json body
    try:
        data = json.loads(request.body)
    except json.JSONDecodeError:
        return HttpResponseBadRequest("Invalid JSON body")
    
    meal_id = data.get("meal_id")
    rating = data.get("rating")
    is_good_value = data.get("is_good_value")
    is_good_service = data.get("is_good_service")
    is_crowded = data.get("is_crowded")
    is_refillable = data.get("is_refillable")
    is_large_portion = data.get("is_large_portion")
    user_id = data.get("user_id")

    # Validate input
    if meal_id is None:
        return HttpResponseBadRequest("Missing parameter meal_id")
    
    if rating is None:
        return HttpResponseBadRequest("Missing parameter rating")
    
    # set defaults
    if is_good_value is None:
        is_good_value = False
    if is_good_service is None:
        is_good_service = False
    if is_crowded is None:
        is_crowded = False
    if is_refillable is None:
        is_refillable = False
    if is_large_portion is None:
        is_large_portion = False

    try:
        meal_id = int(meal_id)
    except ValueError:
        return HttpResponseBadRequest("Invalid meal_id")
    
    try:
        meal = Meal.objects.get(id=meal_id)
    except Meal.DoesNotExist:
        return HttpResponseBadRequest("Invalid meal_id")
    
    if user_id is not None:
        try:
            user_id = int(user_id)
            try:
                user = WelpUser.objects.get(id=user_id)
            except WelpUser.DoesNotExist:
                return HttpResponseBadRequest("Invalid user_id : User does not exist")
        except ValueError:
            return HttpResponseBadRequest("Invalid user_id")
    
    
    review = Review(meal=meal, rating=rating, is_good_value=is_good_value, is_good_service=is_good_service, is_crowded=is_crowded, is_refillable=is_refillable, is_large_portion=is_large_portion)
    review.save()
    
    return HttpResponse("OK")


"""
Hook to populate the db
"""
def populate(request):
    if request.method != 'GET':
        return HttpResponseBadRequest("Invalid method - Use GET")
    
    if not (request.user.is_authenticated and request.user.is_superuser):
        return HttpResponseBadRequest("Invalid user - Must be superuser")
    
    from api.management.commands.populate import Command
    command = Command()
    command.handle()
    return HttpResponse("OK")

"""
Hook to create the session
"""
def create_session(request):
    if request.method != 'GET':
        return HttpResponseBadRequest("Invalid method - Use GET")
    
    if not (request.user.is_authenticated and request.user.is_superuser):
        return HttpResponseBadRequest("Invalid user - Must be superuser")
    
    from api.management.commands.create_session import Command
    command = Command()
    command.handle()
    return HttpResponse("OK")

    

