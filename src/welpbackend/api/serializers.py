from rest_framework import serializers
from .models import Mensa, Menu, Meal, ReviewSession, Review

class MensaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mensa
        fields = ['id', 'display_name', 'location_x', 'location_y', 'organization', 'campus', 'primary_opening_time', 'primary_closing_time', 'secondary_opening_time', 'secondary_closing_time']

class MealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meal
        fields = ['id', 'menu', 'brand', 'name', 'long_description', 'price_student', 'price_internal', 'price_external', 'is_vegetarian', 'score', 'value_score', 'service_score', 'crowded_score', 'refillable_score', "portion_score", "image_url"]

class ShortMealSerializer(serializers.ModelSerializer):
    """
    Used for refreshing scores, only provides the id and the scores.
    """
    class Meta:
        model = Meal
        fields = ['id', 'score', 'value_score', 'service_score', 'crowded_score']

class MenuSerializer(serializers.ModelSerializer):
    meals = MealSerializer(many=True, read_only=True)
    associated_mensa = MensaSerializer(read_only=True)

    class Meta:
        model = Menu
        fields = ['id', 'associated_mensa', 'session', 'meals']

    def get_queryset(self):
        """
        Only return the menus for the current session
        """
        return Menu.objects.filter(session=ReviewSession.objects.get_current_session())

class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ['id', 'meal', 'rating', 'is_good_value', 'is_good_service', 'is_crowded', 'is_refillable', 'is_large_portion', 'user']