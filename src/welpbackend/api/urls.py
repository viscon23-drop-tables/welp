from django.urls import include, path
from rest_framework import routers
from api import views

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'mensas/?', views.MensasViewSet)
router.register(r'currentRankings/?', views.RankedMealViewSet, basename='currentRankings')
router.register(r'menus/?', views.MenusViewSet, basename='menus')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('review', views.post_review, name='post_review'),
    path('createSession', views.create_session, name='create_session'),
    path('populate', views.populate, name='populate'),

]