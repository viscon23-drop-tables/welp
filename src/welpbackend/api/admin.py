from django.contrib import admin

"""
Create the Mensa admin page
"""
from api.models import Mensa, Review, Meal, Menu, Month, ReviewSession

class MensaAdmin(admin.ModelAdmin):
    list_display = ('id', 'display_name', 'location_x', 'location_y', 'organization', 'campus', 'primary_opening_time', 'primary_closing_time', 'secondary_opening_time', 'secondary_closing_time')
    list_editable = ('display_name', 'location_x', 'location_y', 'organization', 'campus', 'primary_opening_time', 'primary_closing_time', 'secondary_opening_time', 'secondary_closing_time')


class MealAdmin(admin.ModelAdmin):
    list_display = ('id', 'menu', 'brand', 'name', 'long_description', 'price_student', 'price_internal', 'price_external', 'is_vegetarian', 'score', 'value_score', 'service_score', 'crowded_score', 'refillable_score', "portion_score", "image_url")
    list_editable = ('menu', 'brand', 'name', 'long_description', 'price_student', 'price_internal', 'price_external', 'is_vegetarian')

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'meal', 'rating', 'is_good_value', 'is_good_service', 'is_crowded', 'is_refillable', 'is_large_portion', 'user')

admin.site.register(Mensa, MensaAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Meal, MealAdmin)