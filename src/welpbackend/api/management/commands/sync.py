import time
import requests
import json
from datetime import datetime, timedelta
import urllib.parse

from api.models import Meal, Mensa, ReviewSession

def get_all_eth_menus():

    current_session = ReviewSession.get_current_session()

    today = datetime.utcnow()

    # add 2 days for debug
    today = today + timedelta(days=2)

    today = today.strftime('%Y-%m-%d')
    url = f"https://idapps.ethz.ch/cookpit-pub-services/v1/weeklyrotas?client-id=ethz-wcms&lang=en&rs-first=0&rs-size=50&valid-after={today}"

    print(url)
    response = requests.get(url)

    if response.status_code != 200:
        return None

    responseJson = json.loads(response.content)

    """
    Get the opening times for each facility
    """

    day_of_week_nr = datetime.utcnow().weekday()

    for facility in responseJson["weekly-rota-array"]:
        id = facility["facility-id"]
        print(id)
        try:
            day_json = facility["day-of-week-array"][day_of_week_nr - 1]["opening-hour-array"][0]["meal-time-array"]
        except:
            print("Could not get opening hours for " + str(id))
            continue
        for meal_time in day_json:
            if meal_time['time-to'] == None:
                continue

            # Convert the time to a datetime object
            time_from = datetime.strptime(meal_time['time-from'], '%H:%M')
            time_to = datetime.strptime(meal_time['time-to'], '%H:%M')

            # If the time is not in the correct range, skip it
            if (not current_session.is_dinner) and time_from.hour > 16:
                try:
                    mensa = Mensa.objects.get(id=id)
                    mensa.secondary_opening_time = time_from.strftime('%H:%M')
                    mensa.secondary_closing_time = time_to.strftime('%H:%M')
                    mensa.save()
                except:
                    continue
                continue

            mensa = Mensa.objects.get(id=id)
            mensa.primary_opening_time = time_from.strftime('%H:%M')
            mensa.primary_closing_time = time_to.strftime('%H:%M')
            mensa.save()

            # Create the menu
            menu = current_session.menu_set.get_or_create(mensa=mensa)[0]

            # Create the meals
            try:
                if meal_time["line-array"] == None:
                    print("No meals for " + str(id))
                    continue
            except:
                print("No meals for " + str(id))
                continue
            
            for meal in meal_time["line-array"]:
                try:
                    brand = meal["name"]
                    details = meal['meal']
                    name = details['name']
                    description = details['description']

                    prices = details['meal-price-array']
                    price_student = prices[0]['price']
                    price_internal = prices[1]['price']
                    price_external = prices[2]['price']

                    meal_class = details['meal-class-array']
                    is_vegetarian = meal_class[0]['code'] == 17

                    meal = Meal.objects.get_or_create(menu=menu, name=name)[0]
                    meal.brand = brand
                    meal.long_description = description
                    meal.price_student = price_student
                    meal.price_internal = price_internal
                    meal.price_external = price_external
                    meal.is_vegetarian = is_vegetarian
                    meal.save()
                    print(meal)
                except Exception as e:
                    print("Could not create meal for " + str(id) + ": " + str(e))


                








    return responseJson




def get_ETH_facilities():

    url = "https://idapps.ethz.ch/cookpit-pub-services/v1/facilities?client-id=ethz-wcms&lang=en&rs-first=0&rs-size=50"

    response = requests.get(url)

    if response.status_code != 200:
        return None
    
    responseJson = json.loads(response.content)

    ## JSON format:
    """
    {
        "facility-array": [
            {
                "facility-id": 12,
                "facility-name": "Eureka Take Away",
                "address-line-2": "Gloriastrasse 37/39",
                "address-line-3": "8006 Zurich",
            },
            ...

    
    """

    for facility in responseJson["facility-array"]:
        id = facility["facility-id"]
        name = facility["facility-name"]
        address = facility["address-line-2"] + ", " + facility["address-line-3"]
        organization = "ETH"


        url = 'https://geocode.maps.co/search?q=' + urllib.parse.quote(address)
        print(url)

        response = requests.get(url).json()
        """
        Get the first element in the list of results, and get the lat and long
        """
        try:
            lat = float(response[0]["lat"])
            lon = float(response[0]["lon"])
            print(lat, lon)

            long_zentrum = 8.547320748062326
            long_hongg = 8.5073804

            campus = "zentrum" if abs(lon - long_zentrum) < abs(lon - long_hongg) else "honggerberg"

            mensa = Mensa(id=id, display_name=name, location_x=lat, location_y=lon, organization=organization, campus=campus)
            mensa.save()
        except:
            print("Could not get lat and long for " + name)
            mensa = Mensa(id=id, display_name=name, organization=organization)
            mensa.save()

        time.sleep(1)

        