import os
import random
import time
from django.core.management.base import BaseCommand
from api.management.commands.sync import *
from api.models import *

class Command(BaseCommand):
    help = 'Creates a new ReviewSession'

    def handle(self, *args, **options):
        os.environ['TZ'] = 'Europe/Zurich'
        time.tzset()

        # Check if there is already a Month object for the current month
        # If there is, use that one
        target_year = time.localtime().tm_year
        target_month = time.localtime().tm_mon
        print(target_year, target_month)

        month = Month.objects.get_or_create(year=target_year, month=target_month)[0]

        # Create two ReviewSessions for the current day in the zurich time zone
        day_of_month = time.localtime().tm_mday
        day_name = time.strftime("%A", time.localtime())
        rs1 = ReviewSession.objects.get_or_create(day_of_month=day_of_month, day_name=day_name, month=month, is_dinner=False)[0]
        rs2 = ReviewSession.objects.get_or_create(day_of_month=day_of_month, day_name=day_name, month=month, is_dinner=True)[0]

        # Create a Menu for each Mensa for each ReviewSession
        for mensa in Mensa.objects.all():
            menu1 = Menu.objects.get_or_create(mensa=mensa, session=rs1)[0]
            menu2 = Menu.objects.get_or_create(mensa=mensa, session=rs2)[0]

        get_all_eth_menus()


