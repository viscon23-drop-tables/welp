import csv
import random
from django.core.management.base import BaseCommand, CommandError
from api.models import Meal, Mensa, Review
from api.management.commands.sync import get_ETH_facilities


class Command(BaseCommand):
    help = "Populates the db with some test data"

    def handle(self, *args, **options):
        
        """
        For each mensa, we will assign 
        """

        #get_ETH_facilities()

        # generate random reviews
        #
        for meal in Meal.objects.all():
            for _ in range(random.randint(0, 100)):
                r_rating = random.random()
                r_is_good_value = random.random() > 0.5
                r_is_good_service = random.random() > 0.5
                r_is_crowded = random.random() > 0.5
                r_is_refillable = random.random() > 0.5
                r_is_large_portion = random.random() > 0.5

                review = Review(
                    meal=meal,
                    rating=r_rating,
                    is_good_value=r_is_good_value,
                    is_good_service=r_is_good_service,
                    is_crowded=r_is_crowded,
                    is_refillable=r_is_refillable,
                    is_large_portion=r_is_large_portion,
                )
                review.save()
