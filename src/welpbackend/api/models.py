import os
import time
from typing import Optional
from django.db import models

from accounts.models import WelpUser

class Mensa(models.Model):
    id = models.IntegerField(primary_key=True, unique=True, editable=True)
    display_name = models.CharField(max_length=200, editable=True, unique=True)
    location_x = models.DecimalField(max_digits=22, decimal_places=16, blank=True, null=True, editable=True)
    location_y = models.DecimalField(max_digits=22, decimal_places=16, blank=True, null=True, editable=True)
    organization = models.CharField(max_length=200, editable=True)
    campus = models.CharField(max_length=200, editable=True) #Höng or Zentrum
    primary_opening_time = models.CharField(max_length=200, editable=True, blank=True, null=True)
    primary_closing_time = models.CharField(max_length=200, editable=True, blank=True, null=True)
    secondary_opening_time = models.CharField(max_length=200, editable=True, blank=True, null=True)
    secondary_closing_time = models.CharField(max_length=200, editable=True, blank=True, null=True)

    def __str__(self) -> str:
        return self.display_name
    

class Month(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()

class ReviewSession(models.Model):
    """
    Represents a review session for either a Lunch session or a dinner session, on a given day.
    
    """
    day_of_month = models.IntegerField()
    day_name = models.CharField(max_length=200)
    month = models.ForeignKey(Month, on_delete=models.CASCADE)
    is_dinner = models.BooleanField(default=False)

    @staticmethod
    def get_current_session() -> 'ReviewSession':
        """
        if the time is between 00:00 and 16:00, return the lunch session for today
        if the time is between 16:00 and 23:59, return the dinner session for today
        """
        os.environ['TZ'] = 'Europe/Zurich'
        time.tzset()
        hour = time.localtime().tm_hour
        if hour < 16:
            return ReviewSession.objects.get_or_create(day_of_month=time.localtime().tm_mday, day_name=time.strftime("%A", time.localtime()), month=Month.objects.get_or_create(year=time.localtime().tm_year, month=time.localtime().tm_mon)[0], is_dinner=False)[0]
        else:
            return ReviewSession.objects.get_or_create(day_of_month=time.localtime().tm_mday, day_name=time.strftime("%A", time.localtime()), month=Month.objects.get_or_create(year=time.localtime().tm_year, month=time.localtime().tm_mon)[0], is_dinner=True)[0]

    def __str__(self) -> str:
        return "review session: " + str(self.day_name) + " " + str(self.day_of_month) + " " + str(self.month) + " " + ("dinner" if self.is_dinner else "lunch")

class Menu(models.Model):
    # Direct link to mensa
    mensa = models.ForeignKey(Mensa, on_delete=models.CASCADE)
    session = models.ForeignKey(ReviewSession, on_delete=models.CASCADE)

    def associated_mensa(self) -> Mensa:
        """
        Returns the mensa that this menu is associated with.
        """
        return self.mensa
    
    def __str__(self) -> str:
        return "MENU : " + str(self.mensa) + ", " + str(self.session)



class Meal(models.Model):
    # Link to a specific menu
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE, related_name="meals")
    name = models.CharField(max_length=200, blank=True, null=True)
    brand = models.CharField(max_length=200, blank=True, null=True)
    long_description = models.CharField(max_length=500, blank=True, null=True)

    price_student = models.FloatField(default=0.0)
    price_internal = models.FloatField(default=0.0)
    price_external = models.FloatField(default=0.0)

    is_vegetarian = models.BooleanField(default=False)

    image_url = models.CharField(max_length=200, blank=True, null=True)

    def score(self) -> Optional[float]:
        """
        Returns the score of this meal, based on the reviews of this meal.
        """
        reviews = self.reviews.all()
        if len(reviews) == 0:
            return -1
        return sum([review.rating for review in reviews]) / len(reviews)
    
    def value_score(self) -> Optional[float]:
        """
        Returns the value score of this meal, based on the reviews of this meal.
        """
        reviews = self.reviews.all()
        if len(reviews) == 0:
            return None
        return sum([review.is_good_value for review in reviews]) / len(reviews)
    
    def service_score(self) -> Optional[float]:
        """
        Returns the service score of this meal, based on the reviews of this meal.
        """
        reviews = self.reviews.all()
        if len(reviews) == 0:
            return None
        return sum([review.is_good_service for review in reviews]) / len(reviews)
    
    def crowded_score(self) -> Optional[float]:
        """
        Returns the crowded score of this meal, based on the reviews of this meal.
        """
        reviews = self.reviews.all()
        if len(reviews) == 0:
            return None
        return sum([review.is_crowded for review in reviews]) / len(reviews)
    
    def refillable_score(self) -> Optional[float]:
        """
        Returns the refillable score of this meal, based on the reviews of this meal.
        """
        reviews = self.reviews.all()
        if len(reviews) == 0:
            return None
        return sum([review.is_refillable for review in reviews]) / len(reviews)
    
    def portion_score(self) -> Optional[float]:
        """
        Returns the portion score of this meal, based on the reviews of this meal.
        """
        reviews = self.reviews.all()
        if len(reviews) == 0:
            return None
        return sum([review.is_large_portion for review in reviews]) / len(reviews)
    
    def associated_mensa(self) -> Mensa:
        """
        Returns the mensa that this meal is associated with.
        """
        return self.menu.mensa
    
    def __str__(self) -> str:
        return "meal : [" + str(self.menu.mensa) + "] : " + str(self.name) 

class Review(models.Model):
    """
    Represents a review of a specific meal, by a specific user, in a specific session.
    """
    user = models.ForeignKey(WelpUser, on_delete=models.CASCADE, null=True, blank=True)
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE, related_name="reviews")
    rating = models.FloatField()
    is_good_value = models.BooleanField()
    is_good_service = models.BooleanField()
    is_large_portion = models.BooleanField()
    is_refillable = models.BooleanField()
    is_crowded = models.BooleanField()

    