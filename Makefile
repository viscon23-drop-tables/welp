start_backend: poetry_install
	@echo "Starting backend..."
	cd src/welpbackend; poetry run python3 manage.py runserver 127.0.0.1:5001

poetry_install:
	@echo "Installing poetry..."
	@pip install poetry